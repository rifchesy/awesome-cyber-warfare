# DNS Tools

* [cjdns](https://github.com/cjdelisle/cjdns/) - Advanced mesh routing system with cryptographic addressing
* [djbdns](https://cr.yp.to/djbdns.html) - D.J. Bernstein's DNS tools
* [dns2tcp](https://packages.debian.org/sid/dns2tcp) - TCP over DNS tunnel
* [dnscontrol](https://github.com/StackExchange/dnscontrol) - It is system for maintaining DNS zones
* [dnscrypt-proxy](https://github.com/jedisct1/dnscrypt-proxy) - Secure communications between a client and a DNS resolver
* [dnscrypt-wrapper](https://cofyc.github.io/dnscrypt-wrapper/) - Server-side proxy that adds dnscrypt support to name resolvers
* [dnsdist](https://www.dnsdist.org/) - Highly DNS-, DoS- and abuse-aware loadbalancer
* [dnsmap](https://code.google.com/archive/p/dnsmap/) - Passive DNS network mapper (a.k.a. subdomains bruteforcer)
* [dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html) - Lightweight DNS forwarder and DHCP server
* [dnsperf](https://www.dns-oarc.net/tools/dnsperf) - Measure DNS performance by simulating network conditions
* [dnsrecon](https://github.com/darkoperator/dnsrecon) - DNS Enumeration Script
* [dnsrend](http://romana.now.ie/dnsrend) - DNS message dissector
* [dnstop](http://dns.measurement-factory.com/tools/dnstop/index.html) - Console tool to analyze DNS traffic
* [dnstracer](https://www.mavetju.org/unix/dnstracer.php) - Trace a chain of DNS servers to the source
* [dnstwist](https://github.com/elceef/dnstwist) - Test domains for typo squatting, phishing and corporate espionage
* [dnsviz](https://github.com/dnsviz/dnsviz/) - Tools for analyzing and visualizing DNS and DNSSEC behavior
* [getdns](https://getdnsapi.net) - Modern asynchronous DNS API
* [launchdns](https://github.com/josh/launchdns) - Mini DNS server designed solely to route queries to localhost
* [pdnsd](http://members.home.nl/p.a.rombouts/pdnsd/) - Proxy DNS server with permanent caching
* [scrape_dns](https://github.com/304GEEK/Scrape-DNS) - Check DNS servers for interesting cached entries
