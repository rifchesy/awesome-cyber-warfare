# Serverless

* [Serverless Still Runs on Servers](https://lightstep.com/blog/serverless-still-runs-servers/)
* [You are thinking about serverless costs all wrong](https://medium.com/theburningmonk-com/you-are-thinking-about-serverless-costs-all-wrong-82eb51eec92e)
* [Serverless computing: one step forward, two steps back](https://blog.acolyer.org/2019/01/14/serverless-computing-one-step-forward-two-steps-back/)
* [Lessons Learned — A Year Of Going “Fully Serverless” In Production](https://dev.to/ketacode/lessons-learneda-year-of-going-fully-serverless-in-production-3mh8)
