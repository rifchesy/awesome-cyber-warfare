# Privacy and Ad Blocking

## Privacy Enhancing Web Extensions

Here are some helpful privacy extensions that don't take too much effort to setup and almost no effort to use. They have been selected for normal internet users, to provide the best protection from internet tracking with the least amount of work on the part of the user.

Extensions are listed below in the order that normal users should install them. Try installing the first two or three right now. When you're comfortable browsing the web with those extensions, try installing a few more.

Install links provided are for [Firefox](https://www.mozilla.org/en-US/firefox/), though most of the extensions will work in other browsers as well.

### Recommended Extensions

* [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)  - Per-tab firewall, ad blocker and cosmetic filtering ([source code](https://github.com/gorhill/uBlock/))
* [ScriptSafe](https://addons.mozilla.org/en-US/firefox/addon/script-safe/) - Block JavaScript Execution and 15 other types of tracking and profiling ([source code](https://github.com/andryou/scriptsafe))
* [Privacy Badger | Electronic Frontier Foundation](https://www.eff.org/privacybadger) - Algorithmically block cookies and trackers ([source code](https://github.com/EFForg/privacybadger))
* [DuckDuckGo Privacy Essentials](https://addons.mozilla.org/en-US/firefox/addon/duckduckgo-for-firefox/?src=search)  - Lightweight tracker blocking and website privacy ratings ([source code](https://github.com/duckduckgo/duckduckgo-privacy-extension))
* [Decentraleyes](https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/) - Local emulation of Content Delivery Networks to reduce CDN based tracking ([source code](https://git.synz.io/Synzvato/decentraleyes))
* [Facebook Container](https://addons.mozilla.org/en-US/firefox/addon/facebook-container/) - Isolates your Facebook activity from the rest of your web activity in order to prevent Facebook from tracking you outside of the Facebook website ([source code](https://github.com/mozilla/contain-facebook))

Note that all extensions have links to their source code. **Always use open source extensions**, even if you never look at their source code yourself. Closed source extensions have a history of being subverted toward different ends e.g. showing the "good ads" like AdblockPlus and Ghostery.

### Optional Extensions

* [Firefox Lightbeam](https://addons.mozilla.org/en-US/firefox/addon/lightbeam/) - Map and track advertising and tracker networks. This one is more about letting you see the extent of tracking than about stopping it. ([source code](https://github.com/mozilla/lightbeam-we))
* [Temporary Containers](https://addons.mozilla.org/en-US/firefox/addon/temporary-containers/) - An extra layer of isolation for website data. Similar to the Facebook Container extension above, but for all other websites. ([source code](https://github.com/stoically/temporary-containers))

### For Advanced Users

* [uMatrix](https://github.com/gorhill/uMatrix) - Point and click matrix to filter net requests according to source, destination and type
* [Luminous](https://github.com/gbaptista/luminous) - Identify, analyze and block code execution and event collection through JavaScript in your browser with code interception

## Firewalls

### NetGuard

A simple way to block access to the internet per application on Android phones.

* [Website](https://www.netguard.me/)
* [Source Code](https://github.com/M66B/NetGuard/)

### Pi-Hole

Network-wide Ad Blocking - A black hole for Internet advertisements

* [Website](https://pi-hole.net/)
* [Source Code](https://github.com/pi-hole)
* [Troy Hunt: Mmm... Pi-hole...](https://www.troyhunt.com/mmm-pi-hole/)

### AdGuard

Network-wide ads & trackers blocking DNS server

* [Website](https://adguard.com/en/welcome.html)
* [Source Code](https://github.com/AdguardTeam)

## Reading Material

* [Obfuscation | The MIT Press](https://mitpress.mit.edu/books/obfuscation)
* [Investigating Apps interactions with Facebook on Android | Privacy International](https://privacyinternational.org/campaigns/investigating-apps-interactions-facebook-android) - [full report](https://privacyinternational.org/campaigns/investigating-apps-interactions-facebook-android/node/2647), [video](https://media.ccc.de/v/35c3-9941-how_facebook_tracks_you_on_android)

## Ad Chaffing

Chaffing is the practice of adding noise to datasets to hide or obscure activity.

**WARNING**: Running ad chaffers carries security –and potentially legal– risks. DO NOT RUN these without fully understanding these risks.

### TrackMeNot

An artware browser add-on to protect privacy in web-search. By issuing randomized queries to common search-engines, TrackMeNot obfuscates your search profile(s) and registers your discontent with surreptitious tracking.

* [Website](https://cs.nyu.edu/trackmenot/)
* [Source Code](https://github.com/vtoubiana/TrackMeNot)

### AdNauseam

AdNauseam is a lightweight browser extension that blends software tool and artware intervention to actively fight back against tracking by advertising networks. AdNauseam works like an ad-blocker (it is built atop [uBlock Origin](https://github.com/gorhill/uBlock)) to silently simulate clicks on each blocked ad, confusing trackers as to one's real interests. At the same time, AdNauseam serves as a means of amplifying discontent with advertising networks that disregard privacy and enable bulk surveillance.

* [Website](https://adnauseam.io/)
* [Source Code](https://github.com/dhowe/AdNauseam/)
